import rospy
import numpy as np
import sys, select, os
from multiprocessing import Process, Value
from head_and_hand_tracker.msg import HandState
from head_and_hand_tracker.msg import HandHaptics
from isaacgym import gymapi
from isaacgym.torch_utils import *
import torch

class SenseGlove(object):
    def __init__(self, cfg) -> None:
        """Initialise SenseGlove operator.

        Args:
            cfg (Dict): configurations (referenced_tracking, default_pos, default_rot)
        """
        self.referenced_tracking = cfg["referenced_tracking"]
        self.max_flexed = cfg["max_flexed"]
        self.absolute_act = None
        self.relative_act = None

        self.dt = cfg['dt']
        self.control_freq_inv = cfg['control_freq_inv']

        self.pos_scale = 1 / (cfg["hand_pos_limits"][1] - cfg["hand_pos_limits"][0])
        self.rot_scale = 1 / (cfg["hand_rot_limits"][1] - cfg["hand_rot_limits"][0])

        if self.referenced_tracking:
            self.default_pose = {}
            self.default_pose["ee_pos"] = torch.tensor([cfg["default_pos"]["x"], cfg["default_pos"]["y"], cfg["default_pos"]["z"]])
            self.default_pose["ee_rot"] = torch.tensor([cfg["default_rot"]["x"], cfg["default_rot"]["y"], cfg["default_rot"]["z"], cfg["default_rot"]["w"]])
            self.user_ready = Value('b', False)
            self.reference_pose = None
            p = Process(target=self.__wait_for_user_ready, args=(sys.stdin.fileno(), self.user_ready), daemon=True)
            p.start()
        # ROS
        rospy.init_node('senseglove_operator')
        rospy.Subscriber('/handState/right', HandState, self.__move)
        
        self.handHapticsMsg = HandHaptics()
        self.pub = rospy.Publisher('senseglove/right/handHaptics', HandHaptics, queue_size=10)
        
    def __wait_for_user_ready(self, fn, ready):
        """Separate process waiting on user input.

        Args:
            fn (int): stdin file descriptor
            ready (multiprocessing.Value): shared boolean indicating whether the user is ready to start tracking
        """
        sys.stdin = os.fdopen(fn)
        print("Press Space and then Enter once you are ready to start: ")
        inp = ""
        while inp != " \n":
            if sys.stdin in select.select([sys.stdin], [], [], 0.0)[0]:
                inp = sys.stdin.readline(2)
        print("SenseGlove Info: User is ready.")
        ready.value = True
        
    def __scale_flex(self, flexion, finger):
        """Scale finger flexions, convert to space of [-1, 1].
        
        Args:
            flexion (float): normalized Flexion of one SenseGlove finger
            finger (str): one of "thumb", "index", "middle", "ring"
        
        Returns:
            float: scaled finger flex control
        """
        if flexion > self.max_flexed[finger]:
            scaled = 2
        else:
            scaled = flexion * 2/self.max_flexed[finger]
        if finger != "thumb":
            return -1 * scaled + 1
        else:
            return scaled - 1

    def __get_angles(self, hand_angles):
        """Get relevant angles and convert flexion to space of [-1, 1].

        Args:
            hand_angles (head_and_hand_tracker.msg.HandAngles): all finger angles

        Returns:
            list: coupled joint control
        """
        angles = [self.__scale_flex(hand_angles.index.normalizedFlexion, "index"),
                  self.__scale_flex(hand_angles.middle.normalizedFlexion, "middle"),
                  self.__scale_flex(hand_angles.ring.normalizedFlexion, "ring"),
                  float(np.clip(hand_angles.thumb.position[0]*(-4/np.pi)-1, -1, 1)), # 0 radians -> -1, -pi/2 radians -> 1
                  self.__scale_flex(hand_angles.thumb.normalizedFlexion, "thumb")]
        return angles

    def __scale_angle(self, flexion, finger):
        """Scale finger flexions to actual robot joint angles"""
        scaled = -min((flexion / self.max_flexed[finger]) * 1.571, 1.571)
        if finger == "thumb":
            scaled *= -1
        return scaled

    def __get_dof_pos(self, hand_angles):
        dof_pos = torch.tensor([[self.__scale_angle(hand_angles.index.normalizedFlexion, "index"),
                                 self.__scale_angle(hand_angles.middle.normalizedFlexion, "middle"),
                                 self.__scale_angle(hand_angles.ring.normalizedFlexion, "ring"),
                                 max(1.5 * (-1.571-hand_angles.thumb.position[0]), -1.571),  # I put the 1.5 scaling here because I think it is easier to rotate the thumb fully away from the hand this way.
                                 self.__scale_angle(hand_angles.thumb.normalizedFlexion, "thumb")]])
        return dof_pos

    def __get_rotation(self, orientation):
        """Absolute tracking: Convert orientation quaternion to [-1, 1].

        Args:
            orientation (list): hand tracker orientation

        Returns:
            list: rotation, length 3, entries of type float
        """
        # in OpenVR: y is up, gym_grasp: z is up
        rotate_back = gymapi.Quat.from_axis_angle(gymapi.Vec3(0, 0, 1), np.deg2rad(180))
        rot = gymapi.Quat(*orientation) * rotate_back
        euler = rot.to_euler_zyx()
        clipped_rot = np.array(euler).clip(-1, 1)
        return clipped_rot.tolist()

    def __quat_rot_error(self, orientation):
        """Reference tracking: Calculate difference between the reference orientation and tracked orientation.

        Args:
            orientation (torch.tensor): tracked orientation as quaternion

        Returns:
            torch.tensor: difference quaternion
        """
        q_tracked = orientation
        q_ref_conj = quat_conjugate(self.reference_pose["ee_rot"])
        q_diff = quat_mul(q_tracked, q_ref_conj)  # might need to normalise, no idea
        return q_diff

    def __apply_rot_error(self, rot_err):
        """ Reference tracking: Apply orientation difference to default pose, convert to Euler rotation, clip to [-1, 1].

        Args:
            rot_err (torch.tensor): difference between reference pose and current tracked pose as quaternion

        Returns:
            list: orientation part of absolute control action as Euler rotation
        """
        q_new = quat_mul(rot_err, self.default_pose["ee_rot"])
        # use gymapi function instead of torchutil as this immediately gives us the correct intervals (torch_utils.get_euler_xyz does %2*PI)
        euler = torch.Tensor(gymapi.Quat(q_new[0], q_new[1], q_new[2], q_new[3]).to_euler_zyx())
        # coordinate system adjust
        euler_swapped = torch.Tensor([euler[0], -euler[2], euler[1]])
        #euler_swapped[0] = torch.clip(euler_swapped[0], -0.5*np.pi, 0.5*np.pi)
        return euler_swapped
    
    def __map_pose(self, position, orientation):
        # Real -> Sim: x -> -y, y -> z, z -> -x
        position_mapped = [position.x, -position.z, position.y]
        # Left-handed coordinate system to right-handed
        orientation_mapped = [orientation.x, orientation.y, orientation.z, orientation.w]
        return position_mapped, orientation_mapped
    
    def __move(self, state_msg):
        """Calculate absolute control action of robot hand.

        Args:
            state_msg (head_and_hand_tracker.msg.HandState): ROS message containing tracked SensGlove pose
        """
        hand_angles = state_msg.angles
        position = state_msg.pose.position
        orientation = state_msg.pose.orientation

        position, orientation = self.__map_pose(position, orientation)
        if self.referenced_tracking and self.user_ready.value:
            if self.reference_pose is None:
                self.reference_pose = {"ee_pos": torch.tensor(position),
                                       "ee_rot": torch.tensor(orientation)}
                print("SenseGlove Info: Got reference pose.")
                print(f"  Reference position: {self.reference_pose['ee_pos']}")
                print(f"  Default position: {self.default_pose['ee_pos']}")
                print(f"  Reference orientation: {self.reference_pose['ee_rot']}")
                print(f"  Default orientation: {self.default_pose['ee_rot']}")
            else:
                # apply position to default position
                self.absolute_pos = (torch.tensor(position) - self.reference_pose["ee_pos"]).unsqueeze(0)
                absolute_pos_act = self.pos_scale * (self.default_pose["ee_pos"] + self.absolute_pos)
                absolute_pos_act = absolute_pos_act.clip(-1, 1)    
                # apply rotation to default rotation
                rot_err = self.__quat_rot_error(torch.tensor(orientation))
                self.absolute_rot = self.__apply_rot_error(rot_err).unsqueeze(0)
                absolute_rot_act = self.rot_scale * self.absolute_rot
                absolute_rot_act = absolute_rot_act.clip(-1, 1)
                # get finger angles of SIH hand
                sih_coupled = torch.tensor([self.__get_angles(hand_angles)])
                self.sih_dof_pos = self.__get_dof_pos(hand_angles)

                
                # compute absolute action
                self.absolute_act = torch.hstack([absolute_pos_act, absolute_rot_act, sih_coupled])  # shape (1, 11)

        elif not self.referenced_tracking:  # absolute tracking
            # y axis up in OpenVR
            ee_pos = torch.tensor([position])
            ee_rot = torch.tensor([self.__get_rotation(orientation)])
            sih_coupled = torch.tensor([self.__get_angles(hand_angles)])
            self.act = torch.hstack((ee_pos, ee_rot, sih_coupled))  # shape (1, 11)

    def reset(self):
        # used to reset the reference pose and get multiple demos in a row
        self.reference_pose = None
        self.user_ready.value = False
        self.absolute_act = None

        p = Process(target=self.__wait_for_user_ready, args=(sys.stdin.fileno(), self.user_ready), daemon=True)
        p.start()
            
    def haptic_feedback(self, buzz, ffb):
        #print("in haptic feedback:")
        #print("buzz:", buzz)
        #print("ffb:", ffb)
        self.handHapticsMsg.thumb.buzz = buzz[0]
        self.handHapticsMsg.index.buzz = buzz[1]
        self.handHapticsMsg.middle.buzz = buzz[2]
        self.handHapticsMsg.ring.buzz = buzz[3]
        self.handHapticsMsg.pinky.buzz = buzz[4]
        
        self.handHapticsMsg.thumb.ffb = ffb[0]
        self.handHapticsMsg.index.ffb = ffb[1]
        self.handHapticsMsg.middle.ffb = ffb[2]
        self.handHapticsMsg.ring.ffb = ffb[3]
        self.handHapticsMsg.pinky.ffb = ffb[4]

        self.handHapticsMsg.duration = self.dt * self.control_freq_inv
        self.pub.publish(self.handHapticsMsg)
