cmake_minimum_required(VERSION 3.0.2)
project(senseglove_for_gymgrasp)

find_package(catkin REQUIRED COMPONENTS
  roscpp
  rospy
  message_generation
  geometry_msgs  
)

catkin_python_setup()

################################################
## Declare ROS messages, services and actions ##
################################################

#generate_messages(
#  DEPENDENCIES
#  geometry_msgs
#  std_msgs
#)

###################################
## catkin specific configuration ##
###################################

catkin_package(
  CATKIN_DEPENDS message_runtime
)

###########
## Build ##
###########

## Specify additional locations of header files
## Your package locations should be listed before other locations
include_directories(
# include
  ${catkin_INCLUDE_DIRS}
)