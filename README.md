# Sense Glove for Gym-Grasp

This is a ROS package enabling the use of a SenseGlove to operate a `gym-grasp` simulation.
It makes use of the [Head and Hand Tracker](https://git.ais.uni-bonn.de/mosbach/head_and_hand_tracker) package.

## Requirements

- ROS
- [`gym-grasp`](https://github.com/maltemosbach/gym-grasp), which in turn requires Isaac Gym
- `gym`

I suggest setting up a `conda` environment like this:

```bash
conda create -n ros-ig ros-noetic-desktop python=3.8 -c robostack -c robostack-experimental -c conda-forge --no-channel-priority --override-channels
conda env update -n ros-ig --file docs/isaacgym_deps.yaml
conda activate ros-ig
conda install -c conda-forge gym
cd ${ISAAC_GYM_PATH}/python
pip install -e .
cd ${GYM_GRASP_PATH}
pip install -e .
```

where you fill in your paths to Isaac Gym and `gym-grasp`.

Also, in `~/.bashrc`, comment out `source /opt/ros/noetic/setup.bash` as this will conflict with the `conda` install of ROS.

If you do not want ROS to run inside `conda`, you need to make Isaac Gym and `gym-grasp` available outside a `conda` environment.

## Installation

In your catkin workspace, `cd src` and clone this code.

Run `catkin_make` in your catkin workspace and then `source devel/setup.bash`.

## Usage

Launch the Head and Hand Tracker in a terminal.

In another terminal, run `rosrun senseglove_for_gymgrasp example.py` or your own simulation.

### Own simulation

To run your own simulation, simply create a `SenseGlove` instance in your program and use its `act` attribute when interacting with a `gym-grasp` environment:

```py
import rospy
from senseglove_teleoperation import SenseGlove
from omegaconf import OmegaConf

# initialise your environment
env = ...

cfg = OmegaConf.load('cfg/config.yaml')
senseglove_operator = SenseGlove(cfg.senseglove)

# run simulation
done = False
while not done:
    try:
        if senseglove_operator.act is not None:  # is None before receiving message
            obs, rew, done, info = env.step(senseglove_operator.act)
    except rospy.ROSInterruptException:
        pass
```

See [`config.yaml`](cfg/config.yaml) for necessary configurations.